package com.picpay.desafio.android.ui.main

import com.picpay.desafio.android.data.response.User

interface MainContract  {
    interface View  {
        fun displayLoading(Loading : Boolean)
        fun displayError()
        fun adapterData(data :List<User>?)

    }

    interface Presenter  {
        fun getContactList()
        fun attachView(mvpView: MainContract.View?)
    }
}