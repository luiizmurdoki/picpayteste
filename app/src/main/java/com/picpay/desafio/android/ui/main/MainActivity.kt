package com.picpay.desafio.android.ui.main

import android.os.Bundle
import android.view.Display
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.picpay.desafio.android.data.service.PicPayService
import com.picpay.desafio.android.R
import com.picpay.desafio.android.data.response.User
import com.picpay.desafio.android.data.serviceGenerator.ServiceGenerator
import com.picpay.desafio.android.ui.main.adapter.UserListAdapter
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity: AppCompatActivity(), MainContract.View {

    private val presenter: MainContract.Presenter by lazy {
        MainPresenter().apply{}
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter.attachView(this@MainActivity)
        presenter.getContactList()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun onPause() {
        super.onPause()
    }

    private  val contactListAdapter: UserListAdapter by lazy {
       val contactListAdapter = UserListAdapter()
        contactRv.adapter = contactListAdapter
        contactRv.layoutManager = LinearLayoutManager(this)
        contactListAdapter
    }

    override fun adapterData(data: List<User>?) {
        if (data != null) {
            contactListAdapter.users = data
        }
    }

    override fun displayError() {
        Toast.makeText(this@MainActivity, getString(R.string.error), Toast.LENGTH_SHORT).show()
    }

    override fun displayLoading(Loading : Boolean){
         if (Loading) contactLoadPb.visibility = View.VISIBLE
         else contactLoadPb.visibility = View.GONE
    }

}
