package com.picpay.desafio.android.ui.main

import android.view.View
import android.widget.Toast
import com.picpay.desafio.android.R
import com.picpay.desafio.android.data.response.User
import com.picpay.desafio.android.data.service.PicPayService
import com.picpay.desafio.android.data.serviceGenerator.ServiceGenerator
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainPresenter : MainContract.Presenter {

    private var view: MainContract.View? = null

    override fun attachView(mvpView: MainContract.View?) {
        view = mvpView
    }

    override fun getContactList(){
        view?.displayLoading(true)
        ServiceGenerator().retrofit.create(PicPayService ::class.java).getUsers().enqueue(object : Callback<List<User>> {
            override fun onResponse(call: Call<List<User>>, response: Response<List<User>>) {
                view?.displayLoading(false)
                 view?.adapterData(response.body()?.toList())
            }

            override fun onFailure(call: Call<List<User>>, t: Throwable) {
                view?.displayLoading(false)
                view?.displayError()

            }
        })
    }
}